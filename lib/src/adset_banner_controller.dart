import 'package:flutter/services.dart';

import 'adset_event_handler.dart';
import 'adset_events.dart';

class AdmobBannerController extends AdsetEventHandler {
  final MethodChannel _channel;

  AdmobBannerController(int id, Function(AdmobAdEvent, Map<String, dynamic>?)? listener)
      : _channel = MethodChannel('adset_flutter/banner_$id'),
        super(listener) {
        if (listener != null) {
          _channel.setMethodCallHandler(handleEvent);
          _channel.invokeMethod('setListener');
        }
      }

  void dispose() {
    _channel.invokeMethod('dispose');
  }
}