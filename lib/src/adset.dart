import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

import 'banner_size.dart';

class Admob {
  static const _channel = MethodChannel('adset_flutter');

  Admob.initialize({String? id, String? key, bool? log, List<String>? deviceIds}) {
    _channel.invokeMethod('initialize',
        {'key': key, 'log': log,"id":id, "deviceIds":deviceIds})
        .then((value) => debugPrint('initialize $value'));
  }

  static Future<bool> requestTrackingAuthorization() async {
    if (!Platform.isIOS) {
      return Future<bool>.value(true);
    }
    final requestTracking = await _channel.invokeMethod('request_tracking_authorization');
    return requestTracking == true;
  }

  static Future<Size> bannerSize(AdmobBannerSize bannerSize) async {
    final rawResult =
        await _channel.invokeMethod('banner_size', bannerSize.toMap);
    if (rawResult == null) {
      throw Exception('banner_size not provided by platform');
    }
    final resultMap = Map<String, num>.from(rawResult);
    return Size(
      resultMap['width']!.ceilToDouble(),
      resultMap['height']!.ceilToDouble(),
    );
  }
}
