import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'adset.dart';
import 'adset_banner_controller.dart';
import 'adset_events.dart';
import 'banner_size.dart';

class AdmobBanner extends StatefulWidget {
  final String adUnitId;
  final AdmobBannerSize adSize;
  final void Function(AdmobAdEvent, Map<String, dynamic>?)? listener;
  final void Function(AdmobBannerController)? onBannerCreated;
  final bool nonPersonalizedAds;

  const AdmobBanner({
    Key? key,
    required this.adUnitId,
    required this.adSize,
    this.listener,
    this.onBannerCreated,
    this.nonPersonalizedAds = false,
  }) : super(key: key);

  @override
  State createState() => _AdBannerState();
}

class _AdBannerState extends State<AdmobBanner> {
  final UniqueKey _key = UniqueKey();
  late AdmobBannerController _controller;
  Future<Size>? adSize;

  @override
  void initState() {
    super.initState();

    if (!widget.adSize.hasFixedSize) {
      adSize = Admob.bannerSize(widget.adSize);
    } else {
      adSize = Future.value(Size(
        widget.adSize.width.toDouble(),
        widget.adSize.height.toDouble(),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Size>(
      future: adSize,
      builder: (context, snapshot) {
        final adSize = snapshot.data;
        if (adSize == null) {
          return const SizedBox.shrink();
        }

        if (defaultTargetPlatform == TargetPlatform.android) {
          return SizedBox.fromSize(
            size: adSize,
            child: AndroidView(
              key: _key,
              viewType: 'adset_flutter/banner',
              creationParams: bannerCreationParams,
              creationParamsCodec: const StandardMessageCodec(),
              onPlatformViewCreated: _onPlatformViewCreated,
            ),
          );
        } else if (defaultTargetPlatform == TargetPlatform.iOS) {
          return SizedBox.fromSize(
            size: adSize,
            child: UiKitView(
              key: _key,
              viewType: 'adset_flutter/banner',
              creationParams: bannerCreationParams,
              creationParamsCodec: const StandardMessageCodec(),
              onPlatformViewCreated: _onPlatformViewCreated,
            ),
          );
        }

        return Text('$defaultTargetPlatform is not yet supported by the plugin');
      },
    );
  }

  void _onPlatformViewCreated(int id) {
    _controller = AdmobBannerController(id, widget.listener);

    if (widget.onBannerCreated != null) {
      widget.onBannerCreated!(_controller);
    }
  }

  Map<String, dynamic> get bannerCreationParams => <String, dynamic>{
    'adUnitId': widget.adUnitId,
    'adSize': widget.adSize.toMap,
    'nonPersonalizedAds': widget.nonPersonalizedAds,
  };
}
