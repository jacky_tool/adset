-dontwarn com.openmediation.sdk.**.*
-keep class com.openmediation.sdk.**{*;}

-keep public class com.google.android.gms.** {
 public *;
}
-keep public class com.google.ads.** {
 public *;
}
-keep class com.adtbid.sdk.** { *;}
-dontwarn com.adtbid.sdk.**