package com.jacky.adset;

import android.content.Context;
import android.util.DisplayMetrics;
import com.anythink.core.api.ATSDK;
import com.anythink.core.api.NetTrafficeCallback;

import java.util.HashMap;
import java.util.Map;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

public class AdsetMethodChannel extends MethodChannel implements MethodChannel.MethodCallHandler {

    private Context context;
    public AdsetMethodChannel(Context context, BinaryMessenger messenger, String name) {
        super(messenger, name);

        this.context = context;
        setMethodCallHandler(this);
    }

    void dispose() {
        setMethodCallHandler(null);
        context = null;
    }

    private void initialize(String id, String key, MethodChannel.Result result) {
        ATSDK.init(context, id, key);//初始化SDK
        if(BuildConfig.DEBUG) {
//      ATSDK.integrationChecking(context);
//      ATSDK.setNetworkLogDebug(true);
//      ATDebuggerUITest.showDebuggerUI(context);
        }
        result.success("init success");
    }

    @Override
    public void onMethodCall(MethodCall call, MethodChannel.Result result) {
        switch (call.method) {
            case "initialize":
                String id = call.argument("id");
                String key = call.argument("key");
//        ATSDK.setNetworkLogDebug(true);
//        String deviceId = "xxxx";
//        ATSDK.setDebuggerConfig(context, deviceId,
//                new ATDebuggerConfig.Builder().setUMPTestDeviceId(deviceId).build());
                ATSDK.checkIsEuTraffic(context, new NetTrafficeCallback() {
                    @Override
                    public void onResultCallback(boolean isEu) {
                        if(isEu && ATSDK.getGDPRDataLevel(context) == ATSDK.UNKNOWN) {
//                            ATSDK.showGDPRConsentDialog(activity, new ATGDPRConsentDismissListener() {
//                                @Override
//                                public void onDismiss(ConsentDismissInfo consentDismissInfo) {
//                                    int level = consentDismissInfo.getDismissType();
//                                    Log.e("TAG", "onDismiss:" + level );
//                                    //其中level的数值为ATSDK.PERSONALIZED或者是ATSDK.NONPERSONALIZED
//                                    ATSDK.setGDPRUploadDataLevel(context, level);
//                                    initialize(id, key, result);
//                                }
//                            });
                        } else {
                            initialize(id, key, result);
                        }
                    }

                    @Override
                    public void onErrorCallback(String s) {
                        result.success("init fail." + s);
                    }
                });
                break;
            case "banner_size" :
                String name = call.argument("name");
                switch (name) {
                    case "SMART_BANNER" : {
                        DisplayMetrics dm = context.getResources().getDisplayMetrics();
                        Map map = new HashMap();
//            map.put("width", AdSize.Banner.getWidthInPixels(context) / dm.density);
//            map.put("height", AdSize.Banner.getHeightInPixels(context) / dm.density);
                        result.success(map);
                    }break;
                    case "ADAPTIVE_BANNER": {
                        Integer width = call.argument("width");
//            AdSize adSize = AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(context, width);
                        Map map = new HashMap();
//            map.put("width", adSize.getWidth());
//            map.put("height", adSize.getHeight());
                        result.success(map);
                    }break;
                    default:
                        result.error("banner_size", "not implemented name", name);
                        break;
                }
                break;
            default:
                result.notImplemented();
                break;
        }
    }
}
