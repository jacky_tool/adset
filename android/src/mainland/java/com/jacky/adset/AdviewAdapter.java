package com.jacky.adset;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.anythink.banner.unitgroup.api.CustomBannerAdapter;
import com.anythink.core.api.ATInitMediation;

import java.util.Map;

import com.kuaiyou.open.AdManager;
import com.kuaiyou.open.BannerManager;
import com.kuaiyou.open.interfaces.AdViewBannerListener;

/**
 * # 将 <Adapter类名> 改为开发者自己的Adapter类名
 * -keep class com.jacky.adset.AdviewAdapter { *;}
 * -keepclassmembers public class com.jacky.adset.AdviewAdapter {
 *     public *;
 * }
 */
public class AdviewAdapter extends CustomBannerAdapter {

    private String key = "";
    private String id = "";
    private View adView;

    @Override
    public View getBannerView() {
        return adView;
    }

    @Override
    public void loadCustomNetworkAd(Context context, Map<String, Object> map, Map<String, Object> map1) {
        this.key = ATInitMediation.getStringFromMap(map, "key");
        this.id = ATInitMediation.getStringFromMap(map, "id");
        boolean showCloseBtn = ATInitMediation.getIntFromMap(map, "showCloseBtn", 1) == 1;
        int time = ATInitMediation.getIntFromMap(map, "refreshTime", 15);
        //Log.d("tag", "id=" + id + ",key=" + key);

        if (TextUtils.isEmpty(this.key) || TextUtils.isEmpty(this.id)) {
            if (this.mLoadListener != null) {
                this.mLoadListener.onAdLoadError("", "key or id is empty.");
            }
        } else {
            BannerManager bannerManager  = AdManager.createBannerAd();
            bannerManager.loadBannerAd(context, key, id, AdManager.BANNER_SMART);
            bannerManager.setShowCloseBtn(showCloseBtn);
            bannerManager.setRefreshTime(time);
            bannerManager.setBannerListener(new NAdViewBannerListener());
            adView = bannerManager.getBannerLayout();
        }
    }

    @Override
    public void destory() {
    }

    @Override
    public String getNetworkPlacementId() {
        return key;
    }

    @Override
    public String getNetworkSDKVersion() {
        return "4.4.6"; //adview.aar的版本号
    }

    @Override
    public String getNetworkName() {
        return "AdviewCN";
    }

    private class NAdViewBannerListener implements AdViewBannerListener {
        @Override
        public void onAdClicked() {
            Log.d("tag", "Adview onAdClicked");
        }

        @Override
        public void onAdDisplayed() {
            Log.d("tag", "Adview onAdDisplayed");
        }

        @Override
        public void onAdReceived() {
            Log.d("tag", "Adview onAdReceived");
            mLoadListener.onAdDataLoaded();
        }

        @Override
        public void onAdFailedReceived(String s) {
            Log.d("tag", "Adview onAdFailedReceived" + s);
            mLoadListener.onAdLoadError("", s);
        }

        @Override
        public void onAdClosed() {
            Log.d("tag", "Adview onAdClosed");
        }
    }
}
