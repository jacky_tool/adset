package com.jacky.adset;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;

import com.anythink.banner.api.ATBannerListener;
import com.anythink.banner.api.ATBannerView;
import com.anythink.core.api.ATAdConst;
import com.anythink.core.api.ATAdInfo;
import com.anythink.core.api.AdError;

import java.util.HashMap;
import java.util.Map;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.platform.PlatformView;

public class AdsetBanner implements PlatformView, MethodChannel.MethodCallHandler {

    private MethodChannel channel;
    private FrameLayout adLayout;
    private ATBannerView mBannerView;

    public AdsetBanner(Context context, BinaryMessenger messenger, int id, HashMap args) {
        channel = new MethodChannel(messenger, "adset_flutter/banner_" + id);
        adLayout = new FrameLayout(context);

        channel.setMethodCallHandler(this);

        String adUnitId = (String) args.get("adUnitId");
        initBannerAd(context, adUnitId, (HashMap) args.get("adSize"));
    }

    private void initBannerAd(Context context, String adUnitId, HashMap size) {
        mBannerView = new ATBannerView(context);
        mBannerView.setPlacementId(adUnitId);

        Integer w = (Integer) size.get("width");
        Integer h = (Integer) size.get("height");

        int width = context.getResources().getDisplayMetrics().widthPixels;//定一个宽度值，比如屏幕宽度
        int height = (int) (1f * width / (w / h));//按照比例转换高度的值
//        Log.e("tag", "w=" + width + ", h=" + height);

        Map<String, Object> localMap = new HashMap<>();
        localMap.put(ATAdConst.KEY.AD_WIDTH, width);
        localMap.put(ATAdConst.KEY.AD_HEIGHT, height);
        mBannerView.setLocalExtra(localMap);

        mBannerView.loadAd();
        adLayout.addView(mBannerView);
    }

        @Override
    public void onMethodCall(MethodCall call, MethodChannel.Result result) {
        switch (call.method) {
            case "setListener":
                mBannerView.setBannerAdListener(createAdListener(channel));
                break;
            case "dispose":
                dispose();
                break;
            default:
                result.notImplemented();
                break;
        }
    }

    @Override
    public View getView() {
        return adLayout;
    }

    @Override
    public void dispose() {
        if(mBannerView != null) {
            mBannerView.destroy();
        }
        mBannerView = null;
        adLayout.setVisibility(View.GONE);
        adLayout.removeAllViews();
        channel.setMethodCallHandler(null);
    }

//    private AdSize getSize(Context context, HashMap size) {
//        Integer width = (Integer) size.get("width");
//        Integer height = (Integer) size.get("height");
//        String name = (String) size.get("name");
//
//        switch (name) {
//            case "BANNER" : return AdSize.BANNER;
//            case "LARGE_BANNER" : return AdSize.LARGE_BANNER;
//            case "MEDIUM_RECTANGLE" : return AdSize.MEDIUM_RECTANGLE;
//            case "FULL_BANNER" : return AdSize.FULL_BANNER;
//            case "LEADERBOARD" : return AdSize.LEADERBOARD;
//            case "SMART_BANNER" : return AdSize.SMART_BANNER;
//            case "ADAPTIVE_BANNER" : return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(context, width);
//            default: return new AdSize(width, height);
//        }
//    }

    private ATBannerListener createAdListener(MethodChannel channel) {
        return new ATBannerListener() {
            @Override
            public void onBannerLoaded() {
                channel.invokeMethod("loaded", null);
            }

            @Override
            public void onBannerFailed(AdError adError) {
                HashMap map = new HashMap();
                map.put("errorCode", adError.getCode());
                map.put("errorDesc", adError.getDesc());
                channel.invokeMethod("failedToLoad", map);
            }

            @Override
            public void onBannerClicked(ATAdInfo atAdInfo) {
                channel.invokeMethod("clicked", null);
            }

            @Override
            public void onBannerShow(ATAdInfo atAdInfo) {
                channel.invokeMethod("opened", null);
            }

            @Override
            public void onBannerClose(ATAdInfo atAdInfo) {
                channel.invokeMethod("closed", null);
            }

            @Override
            public void onBannerAutoRefreshed(ATAdInfo atAdInfo) {

            }

            @Override
            public void onBannerAutoRefreshFail(AdError adError) {
                onBannerFailed(adError);
            }
//            public void onLogImpression(MBridgeIds mBridgeIds) {
//                channel.invokeMethod("impression", null);
//            }
//
//
//            public void onLeaveApp(MBridgeIds mBridgeIds) {
//                channel.invokeMethod("leftApplication", null);
//            }

        };
    }
}