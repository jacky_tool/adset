package com.jacky.adset;

import android.content.Context;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.BinaryMessenger;

/** AdsetPlugin */
public class AdsetPlugin implements FlutterPlugin {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private AdsetMethodChannel channel;

  @Override
  public void onAttachedToEngine(FlutterPluginBinding flutterPluginBinding) {
    Context context = flutterPluginBinding.getApplicationContext();

    BinaryMessenger messenger = flutterPluginBinding.getBinaryMessenger();
    channel = new AdsetMethodChannel(context, messenger, "adset_flutter");

//    interstitialChannel = MethodChannel(messenger, "adset/interstitial")
//    interstitialChannel.setMethodCallHandler(AdmobInterstitial(flutterPluginBinding))
//
//    rewardChannel = MethodChannel(messenger, "adset/reward")
//    rewardChannel.setMethodCallHandler(AdmobReward(flutterPluginBinding))
    flutterPluginBinding.getPlatformViewRegistry()
            .registerViewFactory("adset_flutter/banner", new AdsetBannerFactory(messenger));
  }



  @Override
  public void onDetachedFromEngine(FlutterPluginBinding binding) {
    channel.dispose();
//    interstitialChannel.setMethodCallHandler(null)
//    rewardChannel.setMethodCallHandler(null)
  }
}
