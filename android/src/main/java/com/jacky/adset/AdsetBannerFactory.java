package com.jacky.adset;

import android.content.Context;

import java.util.HashMap;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.StandardMessageCodec;
import io.flutter.plugin.platform.PlatformView;
import io.flutter.plugin.platform.PlatformViewFactory;

public class AdsetBannerFactory extends PlatformViewFactory {

    private BinaryMessenger messenger;

    public AdsetBannerFactory(BinaryMessenger messenger) {
        super(StandardMessageCodec.INSTANCE);
        this.messenger = messenger;
    }

    @Override
    public PlatformView create(Context context, int viewId, Object args) {
        return new AdsetBanner(context,messenger, viewId, (HashMap) args);
    }
}
