package com.jacky.adset;

import android.content.Context;
import android.util.DisplayMetrics;

import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

public class AdsetMethodChannel extends MethodChannel implements MethodChannel.MethodCallHandler {


    private Context context;
    public AdsetMethodChannel(Context context, BinaryMessenger messenger, String name) {
        super(messenger, name);

        this.context = context;
        setMethodCallHandler(this);
    }

    @Override
    public void onMethodCall(MethodCall call, Result result) {
        switch (call.method) {
            case "initialize":
                MobileAds.initialize(context);
                Object obj = call.argument("deviceIds");
                List<String> ids = obj == null ? new ArrayList<>() : (List<String>)obj;
                RequestConfiguration configuration =
                        new RequestConfiguration.Builder()
                                .setTestDeviceIds(ids).build();
                MobileAds.setRequestConfiguration(configuration);
                break;
            case "banner_size" :
                String name = call.argument("name");
                switch (name) {
                    case "SMART_BANNER" : {
                        DisplayMetrics dm = context.getResources().getDisplayMetrics();
                        Map map = new HashMap();
                        map.put("width", AdSize.SMART_BANNER.getWidthInPixels(context) / dm.density);
                        map.put("height", AdSize.SMART_BANNER.getHeightInPixels(context) / dm.density);
                        result.success(map);
                    }break;
                    case "ADAPTIVE_BANNER": {
                        Integer width = call.argument("width");
                        AdSize adSize = AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(context, width);
                        Map map = new HashMap();
                        map.put("width", adSize.getWidth());
                        map.put("height", adSize.getHeight());
                        result.success(map);
                    }break;
                    default:
                        result.error("banner_size", "not implemented name", name);
                        break;
                }
                break;
            default:
                result.notImplemented();
                break;
        }
    }

    void dispose() {
        setMethodCallHandler(null);
        context = null;
    }
}
