package com.jacky.adset;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;

import java.util.HashMap;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.platform.PlatformView;

public class AdsetBanner implements PlatformView, MethodChannel.MethodCallHandler {

    private MethodChannel channel;
    private AdView adView;

    public AdsetBanner(Context context, BinaryMessenger messenger, int id, HashMap args) {
        channel = new MethodChannel(messenger, "adset_flutter/banner_" + id);
        adView = new AdView(context);

        channel.setMethodCallHandler(this);

        adView.setAdSize(getSize(context, (HashMap) args.get("adSize")));
        adView.setAdUnitId((String) args.get("adUnitId"));

        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
        Object npa = args.get("nonPersonalizedAds");
        if(npa == Boolean.TRUE) {
            Bundle extras = new Bundle();
            extras.putString("npa", "1");
            adRequestBuilder.addNetworkExtrasBundle(AdMobAdapter.class, extras);
        }
        adView.loadAd(adRequestBuilder.build());
    }

    @Override
    public void onMethodCall(MethodCall call, MethodChannel.Result result) {
        switch (call.method) {
            case "setListener":
                adView.setAdListener(createAdListener(channel));
                break;
            case "dispose":
                dispose();
                break;
            default:
                result.notImplemented();
                break;
        }
    }

    @Override
    public View getView() {
        return adView;
    }

    @Override
    public void dispose() {
        adView.setVisibility(View.GONE);
        adView.destroy();
        channel.setMethodCallHandler(null);
    }

    private AdSize getSize(Context context,HashMap size) {
        Integer width = (Integer) size.get("width");
        Integer height = (Integer) size.get("height");
        String name = (String) size.get("name");

        switch (name) {
            case "BANNER" : return AdSize.BANNER;
            case "LARGE_BANNER" : return AdSize.LARGE_BANNER;
            case "MEDIUM_RECTANGLE" : return AdSize.MEDIUM_RECTANGLE;
            case "FULL_BANNER" : return AdSize.FULL_BANNER;
            case "LEADERBOARD" : return AdSize.LEADERBOARD;
            case "SMART_BANNER" : return AdSize.SMART_BANNER;
            case "ADAPTIVE_BANNER" : return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(context, width);
            default: return new AdSize(width, height);
        }
    }

    private AdListener createAdListener(MethodChannel channel) {
        return new AdListener() {
            @Override
            public void onAdLoaded() {
                channel.invokeMethod("loaded", null);
            }

            @Override
            public void onAdFailedToLoad(LoadAdError errorCode) {
                HashMap map = new HashMap();
                map.put("errorCode", errorCode.getCode());
                map.put("errorMsg", errorCode.getMessage());
                channel.invokeMethod("failedToLoad", map);
            }

            @Override
            public void onAdClicked() {
                channel.invokeMethod("clicked", null);
            }

            @Override
            public void onAdImpression() {
                channel.invokeMethod("impression", null);
            }

            @Override
            public void onAdOpened() {
                channel.invokeMethod("opened", null);
            }

            @Override
            public void onAdSwipeGestureClicked() {
                channel.invokeMethod("swipeGestureClicked", null);
            }

            @Override
            public void onAdClosed() {
                channel.invokeMethod("closed", null);
            }
        };
    }
}