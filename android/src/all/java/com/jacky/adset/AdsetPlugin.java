package com.jacky.adset;

import android.content.Context;
import android.util.DisplayMetrics;

import androidx.annotation.NonNull;

import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/** AdsetPlugin */
public class AdsetPlugin implements FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;
  private Context context;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    context = flutterPluginBinding.getApplicationContext();

    BinaryMessenger messenger = flutterPluginBinding.getBinaryMessenger();
    channel = new MethodChannel(messenger, "adset_flutter");
    channel.setMethodCallHandler(this);

//    interstitialChannel = MethodChannel(messenger, "adset/interstitial")
//    interstitialChannel.setMethodCallHandler(AdmobInterstitial(flutterPluginBinding))
//
//    rewardChannel = MethodChannel(messenger, "adset/reward")
//    rewardChannel.setMethodCallHandler(AdmobReward(flutterPluginBinding))
    flutterPluginBinding.getPlatformViewRegistry()
            .registerViewFactory("adset_flutter/banner", new AdsetBannerFactory(messenger));
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    switch (call.method) {
      case "initialize":
        MobileAds.initialize(context);
        Object obj = call.argument("deviceIds");
        List<String> ids = obj == null ? new ArrayList<>() : (List<String>)obj;
        RequestConfiguration configuration =
                new RequestConfiguration.Builder()
                        .setTestDeviceIds(ids).build();
        MobileAds.setRequestConfiguration(configuration);
        break;
      case "banner_size" :
        String name = call.argument("name");
        switch (name) {
          case "SMART_BANNER" : {
            DisplayMetrics dm = context.getResources().getDisplayMetrics();
            Map map = new HashMap();
            map.put("width", AdSize.SMART_BANNER.getWidthInPixels(context) / dm.density);
            map.put("height", AdSize.SMART_BANNER.getHeightInPixels(context) / dm.density);
            result.success(map);
          }break;
          case "ADAPTIVE_BANNER": {
            Integer width = call.argument("width");
            AdSize adSize = AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(context, width);
            Map map = new HashMap();
            map.put("width", adSize.getWidth());
            map.put("height", adSize.getHeight());
            result.success(map);
          }break;
          default:
            result.error("banner_size", "not implemented name", name);
            break;
        }
        break;
      default:
        result.notImplemented();
        break;
    }
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
//    interstitialChannel.setMethodCallHandler(null)
//    rewardChannel.setMethodCallHandler(null)
    context = null;
  }
}
