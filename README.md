# adset

## 介绍
flutter 实现广告的聚合类组件

fork from https://pub-web.flutter-io.cn/packages/admob_flutter

## 配置说明

第三方项目需要在根目录的build.gradle添加下面的配置
```
allprojects {
    repositories {
        flatDir {
            dirs 'libs'
            dirs project(':adset').file('libs')
        }
        ...
    }
}
```
在 android build.gradle添加下面的混淆配置
```
proguardFiles 'proguard-rules.pro', project(':adset').file('proguard.txt')
```